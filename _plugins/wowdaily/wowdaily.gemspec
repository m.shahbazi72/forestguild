Gem::Specification.new do |s|
    s.name        = 'wowdaily'
    s.version     = '0.0.1'
    s.date        = '2018-11-29'
    s.summary     = "WoW Daily from wowhead"
    s.description = "special tool"
    s.authors     = ["Nikita Chernyi"]
    s.email       = 'github@rakshazi.me'
    s.files       = ["lib/wowdaily.rb"]
    s.homepage    = 'https://rakshazi.me'
    #s.license     = 'MIT'
end
