---
metatitle: Тактика Ульдир (гер, нормал) WoW BfA
author: Rakshazi
title: Тактика
layout: wiki
actual:
 - 8.0.1
 - 8.1
---

<div class="row">
<div class="col-lg-6" markdown="1">
# Битва за Дазар'Алор

**[Текстовый гайд от noob-club](https://www.noob-club.ru/index.php?board=270.0)** _советую держать под рукой во время РТ_

<div class="embed-responsive embed-responsive-16by9">
  <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/videoseries?list=PLlWRta-R3jpFoQNaO_PMMqFErj1pke09G" allowfullscreen></iframe>
</div>
</div>

<div class="col-lg-6" markdown="1">
<h1 class="text-muted" markdown="1">~~Ульдир~~</h1>

_Рейд патча 8.0.1, ныне не актуален_

<div class="embed-responsive embed-responsive-16by9">
  <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/videoseries?list=PL1RoZvQc_eCZoh8vbLFleSug5IF9HnveP" allowfullscreen></iframe>
</div>
</div>
</div>
